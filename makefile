.PHONY: all check clean default dist distclean help install release
default: all

COMMIT=$(shell test -d .git && (git show 2>/dev/null | head -1 | cut -d' ' -f 2) || cat .tarball-version)
VERBOSE:=@
OUT:=out
PREFIX:=/usr/local
CCACHE:=$(OUT)/ccache
DIST_CLEAN:=.config.make

.config.make: configure makefile
	./configure

include .config.make
include make/install.make

CLEAN:=$(OUT)/.gitignore
$(OUT)/.gitignore:
	@mkdir -p $(@D)
	@echo '*' > $@

SUB_DIRS:=ly5
$(foreach DIR,$(SUB_DIRS),$(eval include $(DIR)/makefile.make))

all: $(CLEAN)

clean:
	rm -f $(CLEAN) $(OPT_CLEAN)

download: $(TARBALL)
	guix download file://$(PWD)/$<

build: $(TARBALL)
	guix build -f package.scm --with-source=$<

dist-clean: clean
	rm -f $(DIST_CLEAN)

check: all
	@echo No tests failed.

help: help-top

install: all

release: all

help:
	@echo

test-music:
	cat ly/display-as-scheme.ly ly/start.ly | lilypond - 2>/dev/null

define HELP_TOP
Usage: make [OPTION]... [TARGET]...

Targets:
  all             update everything
  build           build guix package
  check           run unit tests
  clean           remove all generated stuff in $$(OUT) [$(OUT)]
  download        download tarball into /gnu/store
  dist            create tarball in $(TARBALL)
  distclean       also clean configuration
  install         install in $$(PREFIX) [$(PREFIX)]
  release         make a release
  test-music      make test music
endef
export HELP_TOP
help-top:
	@echo "$$HELP_TOP"
