;;; Ly5 --- GNU LilyPond on html5
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Ly5.
;;;
;;; Ly5 is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Ly5 is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Ly5.  If not, see <http://www.gnu.org/licenses/>.

(define-module (ly5)

  #:use-module (ice-9 optargs)

  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)

  #:use-module (guix download)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix utils)

  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages version-control))

(define-public ly5
  (package
    (name "ly5")
    (version "0.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
    		    "https://gitlab.com/janneke/"
                    name "/archive/v" version ".tar.gz"
                    ))
              (sha256
               (base32 "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"))))
    (build-system gnu-build-system)
    (propagated-inputs `(("guile" ,guile-2.0)))
    (native-inputs `(("git" ,git)
                     ("guile" ,guile-2.0)
                     ("guix" ,guix)
                     ("make" ,gnu-make)))
    (synopsis "GNU LilyPond on html5")
    (description "Ly5 is a web interface for GNU LilyPond")
    (home-page "https://gitlab.com/janneke/ly5")
    (license gpl3+)))
