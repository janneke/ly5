#! /bin/sh
# -*- scheme -*-
exec ${GUILE-guile} --no-auto-compile -C $(pwd)/out/ccache -L $(pwd) -e '(@@ (ly5) main)' -s "$0" ${1+"$@"}
!#

;;; Ly5 Ly5 --- Guile-Guided Ly5-Guessing
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Ly5 Ly5.
;;;
;;; Ly5 Ly5 is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Ly5 Ly5 is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Ly5 Ly5.  If not, see <http://www.gnu.org/licenses/>.

(define-module (ly5)
  #:use-module (ice-9 getopt-long)

  #:use-module (ly5 ly5)
  #:use-module (ly5 misc)
  #:use-module (ly5 config)
  )

(define (parse-opts args)
  (let* ((option-spec
  	  '((help (single-char #\h))
            (verbose (single-char #\v))
            (version (single-char #\V))))
         (options (getopt-long args option-spec
                               #:stop-at-first-non-option #t))
         (help? (option-ref options 'help #f))
         (files (option-ref options '() '()))
         (usage? (and (not help?) (null? files)))
         (verbose? (option-ref options 'verbose #f))
         (version? (option-ref options 'version #f)))
    (or (and version?
             (stdout "l (ly5 ly5) ~a\n" VERSION)
             (exit 0))
        (and (or help? usage?)
             ((or (and usage? stderr) stdout) "\
Usage: l [OPTION] COMMAND

Options:
  -h, --help     display this help and exit
  -v, --verbose  be verbose
  -V, --version  display version information and exit

Commands:
   run

Examples:
  l run
")
             (exit (or (and usage? 2) 0)))
        options)))

(define (main args)
  (let* ((options (parse-opts args))
         (verbose? (option-ref options 'verbose #f))
         (files (option-ref options '() '())))
    (ly5:test)))
