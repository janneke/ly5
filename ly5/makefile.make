$(OUT)/$(DIR)/%.scm: $(DIR)/%.scm.in
	@echo "SED	$< -> $(notdir $@)"
	@mkdir -p $(@D)
	@sed \
		-e 's,@PREFIX@,$(PREFIX),'\
		-e 's,@VERSION@,$(VERSION),'\
		$< > $@

SCM_IN_FILES:=$(filter %.scm.in,\
  $(shell test -d .git && (git ls-files $(DIR)) || find $(DIR)))
include make/guile.make

SCM_FILES:=$(filter %.scm,\
  $(shell test -d .git && (git ls-files $(DIR)) || find $(DIR)))
$(filter-out %misc.go,\
  $(SCM_FILES:$(DIR)/%.scm=$(CCACHE)/$(DIR)/%.go)): $(CCACHE)/$(DIR)/misc.go
include make/guile.make

