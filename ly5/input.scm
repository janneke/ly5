;;; Ly5 --- GNU LilyPond on html5
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Ly5.
;;;
;;; Ly5 is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Ly5 is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Ly5.  If not, see <http://www.gnu.org/licenses/>.

(define-module (ly5 input)
  #:export (start-music
            start-music-string))

(define start-music
  '(begin
     (ly:output-def-set-variable!
      $defaultpaper
      'page-breaking
      ly:one-line-breaking)
     (ly:output-def-set-variable!
      $defaultpaper
      'ragged-right
      #f)
     (ly:output-def-set-variable!
      $defaultpaper
      'ragged-last
      #t)
     (ly:output-def-set-variable!
      $defaultpaper
      'indent
      0)
     (ly:output-def-set-variable!
      $defaultpaper
      'line-width
      799)
     (ly:output-def-set-variable!
      $defaultpaper
      'paper-width
      840)
     (ly:output-def-set-variable!
      $defaultpaper
      'paper-height
      30)
     (ly:output-def-set-variable!
      $defaultpaper
      'left-margin
      1)
     (ly:output-def-set-variable!
      $defaultpaper
      'top-margin
      1)
     (ly:output-def-set-variable!
      $defaultpaper
      'bottom-margin
      20/33)
     (make-music
      'RelativeOctaveMusic
      'input-tag
      0
      'element
      (make-music
       'ContextSpeccedMusic
       'input-tag
       1
       'property-operations
       '()
       'context-id
       "one"
       'context-type
       'Voice
       'element
       (make-music
        'SequentialMusic
        'input-tag
        2
        'elements
        (list (make-music
               'ContextSpeccedMusic
               'input-tag
               3
               'property-operations
               '()
               'context-id
               #f
               'context-type
               'Staff
               'element
               (make-music
                'PropertySet
                'input-tag
                4
                'value
                "clefs.G"
                'symbol
                'clefGlyph))
              (make-music
               'KeyChangeEvent
               'input-tag
               5
               'tonic
               (ly:make-pitch 0 0 0)
               'pitch-alist
               '((0 . 0)
                 (1 . 0)
                 (2 . 0)
                 (3 . 0)
                 (4 . 0)
                 (5 . 0)
                 (6 . 0)))
              (make-music
               'TimeSignatureMusic
               'input-tag
               6
               'beat-structure
               '()
               'denominator
               4
               'numerator
               4)
              (make-music
               'NoteEvent
               'input-tag
               7
               'duration
               (ly:make-duration 2 0 1 1)
               'pitch
               (ly:make-pitch 0 0 0)
               'articulations
               '()
               'tweaks
               '())
              (make-music
               'SkipEvent
               'input-tag
               8
               'duration
               (ly:make-duration 2 0 4 1))
              (make-music
               'ContextSpeccedMusic
               'input-tag
               9
               'property-operations
               '()
               'context-id
               #f
               'context-type
               'Timing
               'element
               (make-music
                'PropertySet
                'input-tag
                10
                'value
                "|."
                'symbol
                'whichBar))))))))
  
  (define start-music-string
    (format #f "~S" start-music))
