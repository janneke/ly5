;;; Ly5 --- GNU LilyPond on html5
;;; Copyright © 2016 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Ly5.
;;;
;;; Ly5 is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU Affero General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Ly5 is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public License
;;; along with Ly5.  If not, see <http://www.gnu.org/licenses/>.

(define-module (ly5 ly5)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-26)

  #:use-module (ice-9 curried-definitions)
  #:use-module (ice-9 iconv)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 pretty-print)
  #:use-module (oop goops)

  #:use-module (ly5 input)
  #:use-module (ly5 misc)
  #:export (ly5:test))

(define (lilypond-engrave expression . count)
  "Send a LISP expression to LilyPond, wait for return value."
  (let* ((sock (socket PF_INET SOCK_STREAM 0))
	 (port 2904)
	 (server (inet-pton AF_INET "127.0.0.1"))
         (message (if (equal? (effective-version) "2.0") expression
                      (string->bytevector expression "UTF-8"))))
    (catch #t
      (lambda ()
        (connect sock AF_INET server port)
        (set-port-encoding! sock "UTF-8")
        (send sock message)
        (let loop ((line (read-line sock 'concat)))
          (if (or (eof-object? line) (equal? line "<EOF>\n")) '()
              (cons (string-trim-right line) (loop (read-line sock 'concat))))))
      (lambda (key . args)
        (case key
          ((system-error)
           (stderr "LilyPond not there yet:~a:~a...\n" key (caddr args))
           (let ((count (if (null? count) 0 (car count))))
             (if (> count 100)
                 '()
                 (begin
                   (usleep (number->integer (* 1000 (expt 1.1 count))))
                   (lilypond-engrave message (1+ count))))))
          (else (apply throw key args)))))))

(define (run-lily)
  (if (not (file-exists? "ly")) (chdir ".."))
  (let ((prefix "/gnu/store/jjwb7mj4zx94dql4mz1fvi5652p0jhwh-lilypond-2.19.33"))
    (setenv "PATH" (string-append prefix "/bin:" (getenv "PATH")))
    (setenv "LILYPOND_DATADIR" (string-append prefix "/share/lilypond/2.19.33")))
  (with-error-to-file "out/server.log"
    (lambda ()
      (open-pipe "lilypond -dbackend=socket ly/server.ly" "r"))))

(define (on-exit lily-pipe)
  (stderr "lily-pipe: ~a\n" lily-pipe)
  (close-port lily-pipe)
  (kill 0 SIGTERM))

(define null-symbol (string->symbol ""))
(define (symbol-null? x) (eq? x null-symbol))
(define (number->symbol x) (string->symbol (number->string x)))

(define ((is? class) o)
  (and (is-a? o class) o))

(define (goops:name o)
  (if (pair? o) (car o)
      (let ((name (string-drop (string-drop-right (symbol->string (class-name (class-of o))) 1) 1)))
        (string->symbol
         (if (string-prefix? "om:" name) (string-drop name 3) name)))))

(define-class <notation-item> ())

(define-class <notation-item-hello> (<notation-item>)
  (name #:accessor .name #:init-value #f #:init-keyword #:name)
  (version #:accessor .version #:init-value #f #:init-keyword #:version))

(define-class <notation-item-x> (<notation-item>)
  (system #:accessor .system #:init-value -1 #:init-keyword #:system)
  (offset #:accessor .offset #:init-value #f #:init-keyword #:offset)
  ;;(fields #:accessor .fields #:init-form '() #:init-keyword #:fields)
  (bbox #:accessor .bbox #:init-value #f #:init-keyword #:bbox)
  ;;(rect #:accessor .rect #:init-value #f #:init-keyword #:rect)
  (cause #:accessor .cause #:init-value #f #:init-keyword #:cause)
  (canvas-item #:accessor .canvas-item #:init-value #f)
  (music #:accessor .music #:init-value #f #:init-keyword #:music))

(define-class <notation-item-glyph> (<notation-item-x>)
  (char #:accessor .char #:init-value 0 #:init-keyword #:char)
  (font #:accessor .font #:init-value #f #:init-keyword #:font)
  (scale #:accessor .scale #:init-value 1 #:init-keyword #:scale)
  (name #:accessor .name #:init-value null-symbol #:init-keyword #:name))
(define-class <notation-item-line> (<notation-item-x>)
  (thick #:accessor .thick #:init-value 0 #:init-keyword #:thick))
(define-class <notation-item-path> (<notation-item-x>))
(define-class <notation-item-polygon> (<notation-item-x>))
(define-class <notation-item-rectangle> (<notation-item-x>))
(define-class <notation-item-text> (<notation-item-x>))
(define-class <notation-item-system> (<notation-item-x>))

(define* (goops:->list o #:optional (marker null-symbol))
  (match o
    ((and (? (is? <object>)) (? (negate pair?)))
     (cons (symbol-append (goops:name o) marker)
           (map goops:->list (goops:children o))))
    ((h t ...) (map goops:->list o))
    (_ o)))

(define-method (goops:children o)
  (map (lambda (slot)
         (slot-ref o (slot-definition-name slot)))
       ((compose class-slots class-of) o)))

(define-class <bbox> ()
  (x1 #:accessor .x1 #:init-value 0 #:init-keyword #:x1)
  (y1 #:accessor .y1 #:init-value 0 #:init-keyword #:y1)
  (x2 #:accessor .x2 #:init-value 0 #:init-keyword #:x2)
  (y2 #:accessor .y2 #:init-value 0 #:init-keyword #:y2))

(define-class <point> ()
  (x #:accessor .x #:init-value 0 #:init-keyword #:x)
  (y #:accessor .y #:init-value 0 #:init-keyword #:y))

(define (list->bbox x1 y1 x2 y2)
  (make <bbox> #:x1 x1 #:y1 y1 #:x2 x2 #:y2 y2))

(define (string->notation o)
  ;;(stderr "string->notation o=~S\n" o)
  (match o
    (((? string?) t ...)
     (filter-map
      (compose string->notation ->symbol (cut string-split <> #\space)) o))
    (('hello name version)
     (make <notation-item-hello> #:name name #:version version))
    (('paper t ...) #f)
    (('system t ...) (make <notation-item-system> #:bbox (apply list->bbox (cdr o))))
    (('cause t ...) #f)
    (('at x y t ...)
     (let ((offset (make <point> #:x x #:y y)))
       (match t
         (('drawline thick x1 y1 x2 y2)
          (make <notation-item-line> #:offset offset #:thick thick #:bbox (apply list->bbox (cddr t))))
         (('draw_round_box x1 x2 y1 y2 thick)
          (make <notation-item-rectangle> #:offset offset #:thick thick #:bbox (apply list->bbox (cddr t))))
         (('glyphshow char font scale name)
          (make <notation-item-glyph> #:offset offset #:char char #:font font #:scale scale #:name name))
         (_ #f))))
    (('nocause) #f)
    ((h t ...) (filter-map string->notation o))
    (_ (stderr  "skipping: ~a\n" o)
       #f)))

(define (ly5:test)
  (let* ((lily-pipe (run-lily))
         (result (lilypond-engrave start-music-string)))
    (stderr "===>\n~a\n" result)
    (pretty-print (goops:->list (string->notation result)) (current-error-port))
    (on-exit lily-pipe)))
