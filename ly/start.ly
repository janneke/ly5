\layout {
  \context {
    \Score
    \override BarNumber  #'break-visibility = #all-visible
  }
}
\relative c' {
  \key es \major
  \time 3/4
  \clef "G"
  r e f d
  \bar "|."
}
